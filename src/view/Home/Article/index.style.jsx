import styled from 'styled-components'

export const ArticleStyle = styled.div`
    width: 100%;
    height: 100%;
    .article {
        width: 100%;
        height: 100%;
        .content {
            width: 700px;
            height: 100%;
            background-color: white;
            margin-left: 300px;
            margin-top: 20px;
            .header {
                width: 700px;
                height: 50px;
                border-bottom: 1px solid #ccc;
            }
            .main {
                width: 700px;
                padding: 0 5px;
                /* height: calc(100% - 50px); */
                overflow-y: auto;
                .list {
                    width: 100%;
                    height: 150px;
                    border-bottom: 1px solid #ccc;
                    display: flex;
                    padding: 10px;
                    .list_item {
                        width: 500px;
                        height: 120px;
                    }
                    & > img {
                        width: 150px;
                        height: 100px;
                        margin-top: 20px;
                    }
                }
            }
        }
    }
`
