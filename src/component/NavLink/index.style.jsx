import styled from 'styled-components'

export const Link = styled.div`
    width: 490px;
    height: 100%;
    display: grid;
    grid-template-columns: repeat(5, 1fr);
    place-items: center;
    a {
        display: block;
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 20px;
        &.selected {
            color: red;
        }
    }
`
