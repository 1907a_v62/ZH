import styled from 'styled-components'

export const Com = styled.div`
    min-height: 100vh;
    .container {
        width: 960px;
        margin-right: auto;
        margin-left: auto;
        ._3tC3y9W6nEYVdAwGypNta9 {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            ._1r_Dp71aY9wU_PZOrRyGzl {
                flex: 1;
                width: calc(100% - 20rem - 1rem);
                padding-top: 1rem;
                padding-bottom: 2rem;
                ._3dE21Z8kljGaURC-u52HoH {
                    width: 100%;
                    overflow: hidden;
                    padding: 1rem;
                    background-color: #eee;
                    border-radius: var(--border-radius);
                    box-shadow: var(--box-shadow);
                    .XifRPckEIhR-ii13RLWBw {
                        position: relative;
                        a {
                            color: #f40a12;
                            text-decoration: none;
                            background-color: transparent;
                            outline: none;
                            cursor: pointer;
                            transition: color 0.3s;
                            -webkit-text-decoration-skip: objects;
                            header {
                                display: flex;
                                align-items: center;
                                ._1vA_Or4uXSUk_Q0pAjXNAk {
                                    color: var(--main-text-color);
                                    overflow: hidden;
                                    font-weight: 600;
                                    font-size: 16px;
                                    line-height: 22px;
                                    text-overflow: ellipsis;
                                    white-space: nowrap;
                                    font-synthesis: style;
                                }
                                .ant-divider-vertical {
                                    position: relative;
                                    top: -0.06em;
                                    display: inline-block;
                                    height: 0.9em;
                                    margin: 0 8px;
                                    vertical-align: middle;
                                    border-top: 0;
                                    border-left: 1px solid rgba(0, 0, 0, 0.06);
                                }
                                .ant-divider {
                                    box-sizing: border-box;
                                    margin: 0;
                                    padding: 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-variant: tabular-nums;
                                    line-height: 1.5715;
                                    list-style: none;
                                    font-feature-settings: 'tnum';
                                    border-top: 1px solid rgba(0, 0, 0, 0.06);
                                }
                                ._37XZHWLrCz-FDEOzkwvIHD {
                                    color: #8590a6;
                                }
                            }
                            main {
                                display: flex;
                                flex-wrap: nowrap;
                                padding-top: 12px;
                                ._1RUkQHMUEKR84DQvX4ZPGy {
                                    flex: 1 1 auto;
                                    display: flex;
                                    flex-direction: column;
                                    justify-content: space-between;
                                    .uzxZQqwvdEFwe6mgNPeYD {
                                        display: -webkit-box;
                                        max-width: 100%;
                                        color: var(--main-text-color);
                                        overflow: hidden;
                                        font-size: 14px;
                                        text-overflow: ellipsis;
                                        -webkit-line-clamp: 3;
                                        -webkit-box-orient: vertical;
                                    }
                                    ._2-5WsQK_Q67xXsuDTBYLqO {
                                        width: 100%;
                                        color: #8590a6;
                                        font-size: 14px;
                                        line-height: 20px;
                                        margin-top: 0.8rem;
                                        .anticon {
                                            display: inline-block;
                                            color: inherit;
                                            font-style: normal;
                                            line-height: 0;
                                            text-align: center;
                                            text-transform: none;
                                            vertical-align: -0.125em;
                                            text-rendering: optimizeLegibility;
                                            -webkit-font-smoothing: antialiased;
                                            -moz-osx-font-smoothing: grayscale;
                                            svg {
                                                display: inline-block;
                                            }
                                            svg:not(:root) {
                                                overflow: hidden;
                                            }
                                        }
                                        .MxfDTpSSoWKL3RwXzbvFj {
                                            margin-left: 6px;
                                            color: var(--second-text-color);
                                        }
                                    }
                                    .aAj27t_TOL5PyHxn0uc08 {
                                        margin: 0 8px;
                                    }
                                    .anticon {
                                        display: inline-block;
                                        color: inherit;
                                        font-style: normal;
                                        line-height: 0;
                                        text-align: center;
                                        text-transform: none;
                                        vertical-align: -0.125em;
                                        text-rendering: optimizeLegibility;
                                        -webkit-font-smoothing: antialiased;
                                        -moz-osx-font-smoothing: grayscale;
                                    }
                                    .MxfDTpSSoWKL3RwXzbvFj {
                                        margin-left: 6px;
                                        color: var(--second-text-color);
                                    }
                                }
                                ._2Cv_sMdeyVvYq6XC91OAQX {
                                    flex: 0 0 auto;
                                    position: relative;
                                    width: 120px;
                                    max-height: 100px;
                                    min-height: 80px;
                                    border-radius: var(--border-radius);
                                    margin-left: 1.5rem;
                                    overflow: hidden;
                                    img {
                                        position: absolute;
                                        top: 50%;
                                        left: 50%;
                                        height: 100%;
                                        width: 100%;
                                        transform: translate3d(-50%, -50%, 0);
                                        object-fit: cover;
                                    }
                                }
                            }
                        }
                        a,
                        a:hover {
                            color: inherit;
                        }
                    }
                    .XifRPckEIhR-ii13RLWBw + .XifRPckEIhR-ii13RLWBw {
                        margin-top: 2rem;
                    }
                }
            }
        }
    }
`
