import { BrowserRouter } from 'react-router-dom'

import './index.global.scss'

import Route from '@/router'

const App = () => (
    <BrowserRouter>
        <Route />
    </BrowserRouter>
)

export default App
