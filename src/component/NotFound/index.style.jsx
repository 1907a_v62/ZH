import styled from 'styled-components'

export const Container = styled.div`
    width: 100%;
    height: 100%;
    background: #e7eaee;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    .content {
        padding: 48px 32px;
        text-align: center;
        z-index: 1;
        .svg-image {
            margin-bottom: 24px;
            text-align: center;
            margin: auto;
            svg {
                transform: scale(1.5);
            }
        }
        .not-title {
            color: #333;
            font-size: 30px;
            line-height: 1.8;
            text-align: center;
            margin-top: 100px;
        }
        .not-subtitle {
            color: rgba(0, 0, 0, 0.85);
            font-size: 20px;
            line-height: 1.6;
            text-align: center;
            margin-top: 10px;
        }
        .not-extra {
            margin: 24px 0 0;
            text-align: center;
            .ant-btn {
                position: relative;
                display: inline-block;
                font-weight: 400;
                white-space: nowrap;
                text-align: center;
                background-image: none;
                box-shadow: 0 2px 0 rgb(0 0 0 / 2%);
                cursor: pointer;
                transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
                user-select: none;
                touch-action: manipulation;
                width: 150px;
                height: 45px;
                line-height: 45px;
                font-size: 18px;
                border-radius: 2px;
                color: #000;
                border: 1px solid #d9d9d9;
                background: #fff;
                color: #fff;
                border-color: #f40a12;
                background: #f40a12;
                text-shadow: 0 -1px 0 rgb(0 0 0 / 12%);
                box-shadow: 0 2px 0 rgb(0 0 0 / 5%);
                margin-top: 15px;
            }
        }
    }
`
