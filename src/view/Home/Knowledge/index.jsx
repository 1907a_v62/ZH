import { Com } from './style'
const Knowledge = () => (
    <Com>
        <div className="container">
            <div className="_3tC3y9W6nEYVdAwGypNta9">
                <section className="_1r_Dp71aY9wU_PZOrRyGzl">
                    <div>
                        <div className="_3dE21Z8kljGaURC-u52HoH">
                            <div className="XifRPckEIhR-ii13RLWBw">
                                <a aria-label="Web 性能指南" href="">
                                    <header>
                                        <div className="_1vA_Or4uXSUk_Q0pAjXNAk">Web 性能指南</div>
                                        <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                                            <div
                                                className="ant-divider ant-divider-vertical"
                                                role="separator"
                                            ></div>
                                            <span className="_37XZHWLrCz-FDEOzkwvIHD">
                                                <time> | 8个月前</time>
                                            </span>
                                        </div>
                                    </header>
                                    <main>
                                        <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                                            <div className="uzxZQqwvdEFwe6mgNPeYD">
                                                Web 性能权威指南阅读笔记
                                            </div>
                                            <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                                <span>
                                                    <span
                                                        role="img"
                                                        aria-label="eye"
                                                        className="anticon anticon-eye"
                                                    >
                                                        <svg
                                                            viewBox="64 64 896 896"
                                                            focusable="false"
                                                            data-icon="eye"
                                                            width="1em"
                                                            height="1em"
                                                            fill="currentColor"
                                                            aria-hidden="true"
                                                        >
                                                            <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                                        </svg>
                                                    </span>
                                                    <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                        3768
                                                    </span>
                                                </span>
                                                <span className="aAj27t_TOL5PyHxn0uc08">.</span>
                                                <span>
                                                    <span>
                                                        <span
                                                            role="img"
                                                            aria-label="share-alt"
                                                            className="anticon anticon-share-alt"
                                                        >
                                                            <svg
                                                                viewBox="64 64 896 896"
                                                                focusable="false"
                                                                data-icon="share-alt"
                                                                width="1em"
                                                                height="1em"
                                                                fill="currentColor"
                                                                aria-hidden="true"
                                                            >
                                                                <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                                            </svg>
                                                        </span>
                                                        <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                            分享
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                                            <img
                                                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-07-24/1_6FIJsfsbLPuRpzxvlGoVWw.png"
                                                alt="cover"
                                            />
                                        </div>
                                    </main>
                                </a>
                            </div>
                            <div className="XifRPckEIhR-ii13RLWBw">
                                <a aria-label="Linux/Unix 编程思想" href="">
                                    <header>
                                        <div className="_1vA_Or4uXSUk_Q0pAjXNAk">
                                            Linux/Unix 编程思想
                                        </div>
                                        <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                                            <div
                                                className="ant-divider ant-divider-vertical"
                                                role="separator"
                                            ></div>
                                            <span className="_37XZHWLrCz-FDEOzkwvIHD">
                                                <time> | 10个月前</time>
                                            </span>
                                        </div>
                                    </header>
                                    <main>
                                        <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                                            <div className="uzxZQqwvdEFwe6mgNPeYD">
                                                Keep it simple,stupid.
                                            </div>
                                            <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                                <span>
                                                    <span
                                                        role="img"
                                                        aria-label="eye"
                                                        className="anticon anticon-eye"
                                                    >
                                                        <svg
                                                            viewBox="64 64 896 896"
                                                            focusable="false"
                                                            data-icon="eye"
                                                            width="1em"
                                                            height="1em"
                                                            fill="currentColor"
                                                            aria-hidden="true"
                                                        >
                                                            <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                                        </svg>
                                                    </span>
                                                    <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                        1205
                                                    </span>
                                                </span>
                                                <span className="aAj27t_TOL5PyHxn0uc08">.</span>
                                                <span>
                                                    <span>
                                                        <span
                                                            role="img"
                                                            aria-label="share-alt"
                                                            className="anticon anticon-share-alt"
                                                        >
                                                            <svg
                                                                viewBox="64 64 896 896"
                                                                focusable="false"
                                                                data-icon="share-alt"
                                                                width="1em"
                                                                height="1em"
                                                                fill="currentColor"
                                                                aria-hidden="true"
                                                            >
                                                                <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                                            </svg>
                                                        </span>
                                                        <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                            分享
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                                            <img
                                                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-05-19/unix-linux.png"
                                                alt="cover"
                                            />
                                        </div>
                                    </main>
                                </a>
                            </div>
                            <div className="XifRPckEIhR-ii13RLWBw">
                                <a aria-label="Python 学习笔记" href="">
                                    <header>
                                        <div className="_1vA_Or4uXSUk_Q0pAjXNAk">
                                            Python 学习笔记
                                        </div>
                                        <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                                            <div
                                                className="ant-divider ant-divider-vertical"
                                                role="separator"
                                            ></div>
                                            <span className="_37XZHWLrCz-FDEOzkwvIHD">
                                                <time> | 大约1年前</time>
                                            </span>
                                        </div>
                                    </header>
                                    <main>
                                        <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                                            <div className="uzxZQqwvdEFwe6mgNPeYD">
                                                Python 学习笔记
                                            </div>
                                            <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                                <span>
                                                    <span
                                                        role="img"
                                                        aria-label="eye"
                                                        className="anticon anticon-eye"
                                                    >
                                                        <svg
                                                            viewBox="64 64 896 896"
                                                            focusable="false"
                                                            data-icon="eye"
                                                            width="1em"
                                                            height="1em"
                                                            fill="currentColor"
                                                            aria-hidden="true"
                                                        >
                                                            <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                                        </svg>
                                                    </span>
                                                    <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                        2118
                                                    </span>
                                                </span>
                                                <span className="aAj27t_TOL5PyHxn0uc08">.</span>
                                                <span>
                                                    <span>
                                                        <span
                                                            role="img"
                                                            aria-label="share-alt"
                                                            className="anticon anticon-share-alt"
                                                        >
                                                            <svg
                                                                viewBox="64 64 896 896"
                                                                focusable="false"
                                                                data-icon="share-alt"
                                                                width="1em"
                                                                height="1em"
                                                                fill="currentColor"
                                                                aria-hidden="true"
                                                            >
                                                                <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                                            </svg>
                                                        </span>
                                                        <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                            分享
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                                            <img
                                                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-03-06/V1.jpg"
                                                alt="cover"
                                            />
                                        </div>
                                    </main>
                                </a>
                            </div>
                            <div className="XifRPckEIhR-ii13RLWBw">
                                <a aria-label="算法于数据结构" href="">
                                    <header>
                                        <div className="_1vA_Or4uXSUk_Q0pAjXNAk">
                                            算法于数据结构
                                        </div>
                                        <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                                            <div
                                                className="ant-divider ant-divider-vertical"
                                                role="separator"
                                            ></div>
                                            <span className="_37XZHWLrCz-FDEOzkwvIHD">
                                                <time> | 大约1年前</time>
                                            </span>
                                        </div>
                                    </header>
                                    <main>
                                        <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                                            <div className="uzxZQqwvdEFwe6mgNPeYD">
                                                使用JavaScript实现算法与数据结构，夯实计算机基础
                                            </div>
                                            <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                                <span>
                                                    <span
                                                        role="img"
                                                        aria-label="eye"
                                                        className="anticon anticon-eye"
                                                    >
                                                        <svg
                                                            viewBox="64 64 896 896"
                                                            focusable="false"
                                                            data-icon="eye"
                                                            width="1em"
                                                            height="1em"
                                                            fill="currentColor"
                                                            aria-hidden="true"
                                                        >
                                                            <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                                        </svg>
                                                    </span>
                                                    <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                        481
                                                    </span>
                                                </span>
                                                <span className="aAj27t_TOL5PyHxn0uc08">.</span>
                                                <span>
                                                    <span>
                                                        <span
                                                            role="img"
                                                            aria-label="share-alt"
                                                            className="anticon anticon-share-alt"
                                                        >
                                                            <svg
                                                                viewBox="64 64 896 896"
                                                                focusable="false"
                                                                data-icon="share-alt"
                                                                width="1em"
                                                                height="1em"
                                                                fill="currentColor"
                                                                aria-hidden="true"
                                                            >
                                                                <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                                            </svg>
                                                        </span>
                                                        <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                            分享
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                                            <img
                                                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-02-22/89TFAVZV4OJTS9NCE0KZFX/3c924980-61ac-11e9-8e4e-6e50e0cec366.png"
                                                alt="cover"
                                            />
                                        </div>
                                    </main>
                                </a>
                            </div>
                            <div className="XifRPckEIhR-ii13RLWBw">
                                <a aria-label="设计模式" href="">
                                    <header>
                                        <div className="_1vA_Or4uXSUk_Q0pAjXNAk">设计模式</div>
                                        <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                                            <div
                                                className="ant-divider ant-divider-vertical"
                                                role="separator"
                                            ></div>
                                            <span className="_37XZHWLrCz-FDEOzkwvIHD">
                                                <time> | 大约1年前</time>
                                            </span>
                                        </div>
                                    </header>
                                    <main>
                                        <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                                            <div className="uzxZQqwvdEFwe6mgNPeYD">
                                                使用TypeScript实现常见设计模式
                                            </div>
                                            <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                                <span>
                                                    <span
                                                        role="img"
                                                        aria-label="eye"
                                                        className="anticon anticon-eye"
                                                    >
                                                        <svg
                                                            viewBox="64 64 896 896"
                                                            focusable="false"
                                                            data-icon="eye"
                                                            width="1em"
                                                            height="1em"
                                                            fill="currentColor"
                                                            aria-hidden="true"
                                                        >
                                                            <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                                        </svg>
                                                    </span>
                                                    <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                        773
                                                    </span>
                                                </span>
                                                <span className="aAj27t_TOL5PyHxn0uc08">.</span>
                                                <span>
                                                    <span>
                                                        <span
                                                            role="img"
                                                            aria-label="share-alt"
                                                            className="anticon anticon-share-alt"
                                                        >
                                                            <svg
                                                                viewBox="64 64 896 896"
                                                                focusable="false"
                                                                data-icon="share-alt"
                                                                width="1em"
                                                                height="1em"
                                                                fill="currentColor"
                                                                aria-hidden="true"
                                                            >
                                                                <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                                            </svg>
                                                        </span>
                                                        <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                            分享
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                                            <img
                                                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-03-06/95185053-ce57a180-07fa-11eb-9d67-baafc83ef280.png"
                                                alt="cover"
                                            />
                                        </div>
                                    </main>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <aside></aside>
            </div>
        </div>
    </Com>
)

export default Knowledge
