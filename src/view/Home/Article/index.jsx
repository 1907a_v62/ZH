import Swipers from '../../../component/swiper/swiper'
import { ArticleStyle } from './index.style'
import { Space } from 'antd'
import { HeartOutlined, EyeOutlined, ShareAltOutlined } from '@ant-design/icons'

const Article = () => (
    <ArticleStyle>
        <div className="article">
            <Swipers></Swipers>
            <div className="content">
                <div className="header"></div>
                <div className="main">
                    <div className="list">
                        <span className="list_item">
                            <b>微信小程序首屏性能优化</b>
                            <br />
                            <br />
                            <p>老生常谈的性能优化，在微信小程序中又应该如何去实现？</p>
                            <p>
                                <Space>
                                    <HeartOutlined />
                                    <span>72</span> <b>·</b>
                                    <EyeOutlined />
                                    <span>3095</span> <b>·</b>
                                    <ShareAltOutlined />
                                    <span>分享</span>
                                </Space>
                            </p>
                        </span>
                        <img
                            src="https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1991065519.jpg"
                            alt=""
                        />
                    </div>
                    <div className="list">
                        <span className="list_item">
                            <b>微信小程序首屏性能优化</b>
                            <br />
                            <br />
                            <p>老生常谈的性能优化，在微信小程序中又应该如何去实现？</p>
                            <p>
                                <Space>
                                    <HeartOutlined />
                                    <span>72</span> <b>·</b>
                                    <EyeOutlined />
                                    <span>3095</span> <b>·</b>
                                    <ShareAltOutlined />
                                    <span>分享</span>
                                </Space>
                            </p>
                        </span>
                        <img
                            src="https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1991065519.jpg"
                            alt=""
                        />
                    </div>
                </div>
            </div>
        </div>
    </ArticleStyle>
)

export default Article
