import NotFound from '@/component/NotFound'

const Notfound = () => (
    <>
        <NotFound />
    </>
)

export default Notfound
